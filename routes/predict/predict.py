from flask import Flask, request, redirect, url_for, flash, jsonify
import numpy as np
import pandas as pd
import pickle as p
import json
from sklearn.linear_model import LogisticRegression
from joblib import dump, load
from __main__ import app

@app.route('/api/predict', methods=['POST'])
def predict():
    data = request.get_json()
    test = pd.DataFrame.from_dict(data)
    model = load('model.joblib') 
    prediction=model.predict(test)
    print(prediction)
    msg= "Model Predictions " + str(prediction)
    return jsonify(msg)


