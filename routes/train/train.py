from flask import Flask, request, redirect, url_for, flash, jsonify
import numpy as np
import pandas as pd
import pickle as p
import json
from sklearn.linear_model import LogisticRegression
from joblib import dump, load
from __main__ import app

@app.route('/api/train', methods=['POST'])
def train():
    data = request.get_json()
    
    df = pd.DataFrame.from_dict(data)

    X = df.iloc[:, :-1]
    y=df.iloc[:,-1]
    print(X)
    print(y)
    lr=LogisticRegression().fit(X, y)
    dump(lr, 'model.joblib') 

    return jsonify("Model trained and saved")

    #sdata = np.array2string(model.predict(data))