from flask import Flask, request, redirect, url_for, flash, jsonify
import numpy as np
import pandas as pd
import pickle as p
import json
from sklearn.linear_model import LogisticRegression
from joblib import dump, load
import os
from __main__ import app

@app.route('/api/delmodel', methods=['DELETE'])
def delmodel():
    data = request.get_json()
    model=data["model"]
    os.remove(model)
    return("Model deleted")
