import flask
from multiprocessing import Value
from flask import Flask, request, jsonify
from __main__ import app

counter = Value('i', 0)
@app.route('/multiple')
def index():
    with counter.get_lock():
        counter.value += 1
    if counter.value<6:
        count=counter.value
        c=str(count)
        return jsonify(c  + " still processing" )
    else:
        return jsonify("success : status code 200")