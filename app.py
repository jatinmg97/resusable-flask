from flask import Flask, request, jsonify
import flask
import json
import time
from dotenv import load_dotenv
load_dotenv()
import os
port = os.getenv("PORT")
host=os.getenv("HOST")
print(host)
app = Flask(__name__)

#importing all the different routes

from routes.get import get
from routes.post import post
from routes.put import put
from routes.multiple import multiple
from routes.test import test 
from routes.delete import delete
from routes.train import train
from routes.predict import predict 
from routes.delmodel import delmodel
from routes.token import token


if __name__ == "__main__":
    app.run(host=host,port=port,debug=True,threaded=True)